// --- Imports ---

const fs = require("fs");
const MongoClient = require("mongodb").MongoClient;
const { request } = require("express");
const cors = require("cors"); // Middleware to handle cors
const express = require("express");
const https = require("https");

// --- Global config ---

const DB_NAME = "server-manager-dev"; // Set db name here

let uri = ""; // This is changed later. Set your URI in a file called dbConfig instead of setting it here.
let client; // MongoDB client. Do not change.

const PRIV_KEY  = fs.readFileSync("certs/key.pem", "utf8");
const CERTIFICATE = fs.readFileSync("certs/cert.pem", "utf8");

const CREDENTIALS = {key: PRIV_KEY, cert: CERTIFICATE};

// --- Express setup ---

const app = express();

app.use(express.json());
app.use(cors());

// --- DB setup ---

// Read dbConfig file for uri so we can use atlas for hosting, then open database connection.

fs.readFile("dbConfig", "utf8", (err, data) => {
  if (err) throw err;
  client = new MongoClient(data, { useNewUrlParser: true, useUnifiedTopology: true }); // Create client so we can access the DB.

  client.connect(err => {
    if (err) {
      throw err;
    }
  });
});

// --- API ---

// "/getServers" Gets all servers and sends in json

app.get("/getServers", (req, res) => {
  let responseCode = 200;

  client.db(DB_NAME).collection("servers").find({}).toArray((err, data) => { // Get all data from database
    if (err) {
      responseCode = 500;
    } else {
      res.send(data); // send all data from DB
    }

    res.status(responseCode); // Set response code
    res.end();
  });
});

// "/addServer": Gets ip and name from post request and puts it in database.

app.post("/addServer", (req, res) => {
  let responseCode = 200;

  // Connect to db and add data
  
  client.db(DB_NAME).collection("servers").insertOne(req.body, (err) => {
    if (err) throw err;

    res.send(req.body);
    res.status(responseCode);
    res.end();
  });
});

// Listen on port 8081, 8080 is used for web server.

const httpsServer = https.createServer(CREDENTIALS, app);
httpsServer.listen(8081);