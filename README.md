# Server Manager

Server Manager is a program used to manage your servers from a simple web interface.

## Components

-  Web server for web interface
-  Server API used to interface with the webserver and be a proxy to the clients
-  Client API to run commands issued by the server API

## Setup

1.  Clone this repository
2.  Run `./getCerts.sh` in the root directory to generate certs
3.  Make a file called `dbConfig` in the `serverAPI` directory and put the URI to your mongoDB instance there
4.  Run `npm install && npm start` in both the `frontend/server` and `serverAPI` directory or use the predefined debug tasks in VS Code
