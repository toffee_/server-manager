// --- Constants ---

let EMPTY_TABLE_BODY = document.createElement("tbody"); // Has to be redefined, is always the same

// --- Globals ---

// Settings

let settings = {
  "form": {
    "apiURLField": document.getElementById("settingsApiURLField"),
  },
};

// API URLS & elements for API form in navbar

const API_URL_FIELD = document.getElementById("navApiURLField");

let getServersURL = "";
let addServersURL = "";

// Others

let serverTable = document.getElementById("serverTable").getElementsByTagName("tbody")[0]; // Server Table

let serverData = {}; // Current server data displayed, used to check for updates
let newData = {}; // New data from getServers when checking for updates

let updateTimer; // Timer for updates

// --- Main Program ---

// Server Table functions

function updateTable(response) {
  // Rows and cells for making the table

  let row;

  let serverCell1; // server name
  let serverCell2; // server IP & URL
  let serverCell3; // status

  serverData = response;

  for (let i = 0; i < response.length; i++) { // Every response
    // console.log(response[i]);

    // Table insert variables

    row = serverTable.insertRow(-1);

    serverCell1 = row.insertCell(0);
    serverCell2 = row.insertCell(1);
    serverCell3 = row.insertCell(2);

    // Add name and IP to row

    serverCell1.innerHTML = `${response[i].serverName}`;
    serverCell2.innerHTML = `<a href="${response[i].serverURL}">${response[i].serverIP}</a>`;
    serverCell3.innerHTML = "<p class='text-warning'>Not Implemented</p>";
  }
}

/* 
removeServers - function - called when new server is detected and after adding server

removerServers replaces the body of serverTable with a blank tbody and redefines serverTable
*/

function removeServers() {
  serverTable.parentNode.replaceChild(EMPTY_TABLE_BODY, serverTable);
  serverTable = document.getElementById("serverTable").getElementsByTagName("tbody")[0];
  EMPTY_TABLE_BODY = document.createElement("tbody");
}

/*
getServers - function - called at start of program

getServers retrieves all the servers from the API using the fetch statement. It then calls a callback to deal with the data.
*/

function getServers(callback) {
  fetch(getServersURL, {})
  .then ((response) => response.json()) // Get JSON from response
  .then ((response) => {
    callback(response);
  });
}

/*
addServer - function - called when user adds server from modal #addServer

addServer gets the form data and sends it to the API which adds a record to the database and then refreshes the data in the server table.
*/

function addServer() {
  let serverURL = "";
  let bodyJSON = {}; // JSON submitted to addServers

  // User-submitted form values

  const serverNameFromForm = document.forms["addServerForm"]["serverName"].value;
  const serverIPFromForm = document.forms["addServerForm"]["serverIP"].value;
  const serverURLFromForm = document.forms["addServerForm"]["serverURL"].value;

  const urlPrefix = document.forms["addServerForm"]["httpSwitcher"].value; // HTTP or HTTPS radios

  if (!serverURLFromForm) { // Checking if a serverURL is defined
    serverURL = urlPrefix + "://" + serverIPFromForm;
  } else {
    serverURL = serverURLFromForm;
    // console.log(serverURL); - Debug
  }

  bodyJSON = { serverName: serverNameFromForm, serverIP: serverIPFromForm, serverURL: serverURL }; // Construct data to send to server
  // console.log(bodyJSON); - Debug

  fetch(addServersURL, { // POST data to addServer on API
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(bodyJSON) // Convert to JSON
  })
  .then ((response) => response.json())
  .then ((response) => {
    // console.log(response); - Debug

    removeServers();
    getServers((response) => {
      updateTable(response);
    });
  });

  return false;
}

/*
checkForUpdates - function - called every 5 seconds

checkForUpdates gets new data using getServers and checks to see if it is different than the old data (serverData). If it is, it will replace the data in the table.
*/

function checkForUpdates() {
  getServers((response) => {
    newData = response

    if (!(_.isEqual(newData, serverData))) {
      removeServers();
      updateTable(newData);
    }
  });

  updateTimer = setTimeout(checkForUpdates, 5000);
}


// Settings functions

function apiURLBuild(bApiURLBase) {
  getServersURL = "https://" + bApiURLBase + "/getServers";
  addServersURL = "https://" + bApiURLBase + "/addServer";

  API_URL_FIELD.placeholder = bApiURLBase;
  API_URL_FIELD.value = "";

  settings["form"]["apiURLField"].placeholder = bApiURLBase;
  settings["form"]["apiURLField"].value = "";

  removeServers();
  
  getServers((response) => {
    updateTable(response);
  });
  
  updateTimer = setTimeout(checkForUpdates, 5000);
}

function loadSettings() {
  apiURLBuild(localStorage.getItem("apiURL"));
}

function saveSettings(sSettings) {
  for (const KEY in sSettings) {
    localStorage.setItem(KEY, sSettings.KEY);
  }
}

function settingsSubmit() {
  saveSettings({ "apiURL": document.forms["settingsForm"]["settingsApiURLField"].value });
  loadSettings();

  return false;
}

function apiURLSubmit() {
  saveSettings({ "apiURL": document.forms["apiURLForm"]["API_URL_FIELD"].value });
  loadSettings();

  return false;
}

loadSettings();