#!/bin/bash

rm frontend/server/certs/*
rm serverAPI/certs/*

cd frontend/server/certs 
openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -days 365 -nodes -subj "/C=US/ST=Oregon/L=Portland/O=Company Name/OU=Org/CN=127.0.0.1"
cd ../../../serverAPI/certs
openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -days 365 -nodes -subj "/C=US/ST=Oregon/L=Portland/O=Company Name/OU=Org/CN=127.0.0.1"